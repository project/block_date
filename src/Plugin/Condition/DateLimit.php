<?php

namespace Drupal\block_date\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Date' condition.
 *
 * @Condition(
 *   id = "date_limit",
 *   label = @Translation("Date"),
 * )
 */
class DateLimit extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Adding date fieldset to the block configure page.
    $form['dates_between'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Date'),
    ];

    $form['dates_between']['from_date'] = [
      '#type' => 'date',
      '#title' => $this->t('From Date'),
      '#default_value' => $this->configuration['dates_between']['from_date'],
      '#description' => $this->t('If you specify only from date, the block will be visible from that date onwards.'),
      '#states' => [
        'required' => [
          ':input[name="visibility[date_limit][dates_between][enable_end_date]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['dates_between']['enable_end_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Collect an End Date'),
      '#default_value' => $this->configuration['dates_between']['enable_end_date'],
      '#description' => $this->t('E.g., Allow this block to appear on September 15, and end on September 16.'),
    ];

    $form['dates_between']['to_date'] = [
      '#type' => 'date',
      '#title' => $this->t('To Date'),
      '#default_value' => $this->configuration['dates_between']['to_date'],
      '#states' => [
        'visible' => [
          ':input[name="visibility[date_limit][dates_between][enable_end_date]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="visibility[date_limit][dates_between][enable_end_date]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['#attached']['library'][] = 'block_date/block_date';
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'dates_between' => [
        'from_date' => '',
        'enable_end_date' => 0,
        'to_date' => '',
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['dates_between'] = $form_state->getValues()['dates_between'];
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {}

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $dates_between = $this->configuration['dates_between'];

    // Get user defined dates.
    $from_date = strtotime($dates_between['from_date']);
    $to_date = strtotime($dates_between['to_date']);
    $enable_end_date = $dates_between['enable_end_date'];

    // Get the current Unix date.
    $now = time();

    // Don't show the block.
    if (!empty($from_date) && $now < $from_date) {
      return FALSE;
    }
    // Don't show the block.
    if (!empty($to_date) && $enable_end_date && $now > $to_date) {
      return FALSE;
    }
    // Show the block.
    return TRUE;
  }

}
